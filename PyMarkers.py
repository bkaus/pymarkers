#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 15:08:33 2021

@author: lucas
"""

import os,sys
from InputGeometry import createGrid
from FDSTAGSavemarkersParallelPython import SaveMarkers
import numpy as np
from gridInterpolator import gridInterpolator
from poly2Phase import poly2Phase


"""
This is an example script on how to save your markerfiles using Python3
    - The geometry is stored in the lamemGrid() class
    - X,Y,Z, phase and temperature are then stored to the markerfiles
    - Phase and temperature a filled with placeholder values and
      can be modified by replacing lamemGrid.Phase and
      lamemGrid.Temp with similar sized arrays
    - If you want to replace the Phase in the markers check out the
      file PyMarkersPhase.py      
"""

inFile = "input/input.dat"

#directory of partitioning file (created with mode save_grid)
partFile ="input/ProcessorPartitioning_4cpu_1.2.2.bin"

#creating the class storing all the information
lamemGrid = createGrid(inFile, partFile)


#save to markerfiles                         
SaveMarkers(partFile,lamemGrid)